detect\_indent.vim
==================

This plugin provides a user command to be run on a buffer after filetype
detection, applying some simple heuristics to adjust indentation settings if
the user's settings don't appear to match the buffer's settings.  It pivots on
the value of `'expandtab'`.

If changing from `'noexpandtab'` to `'expandtab'`, a guess at an appropriate
value for `'shiftround'` is attempted.  It may not be accurate.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
