"
" detect_indent.vim: If 'expandtab' doesn't match the shape of the buffer's
" indents, switch it over, including a guess at 'shiftwidth' if switching it
" on.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_detect_indent') || &compatible || v:version < 700
  finish
endif
let loaded_detect_indent = 1

" Set up a user command in case the user wants to set this manually
command! -bar DetectIndent
      \ call detect_indent#()

" Add hook for FileType event; this should load *after* filetype plugins
augroup detect_indent
  autocmd!
  autocmd FileType *
        \ DetectIndent
augroup END
